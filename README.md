# Validon Capital #



### What is Validon Capital? ###

**Validon Capital** (**VC**)  is a fintech banking startup (neobank) that provides fiat and decentralized financial (**DeFi**) services to the populace, including the underserved and unbanked.